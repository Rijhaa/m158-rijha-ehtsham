# Nextcloud auf Docker - M158 Projekt
Dieses Repository enthält Docker-Konfigurationen für das Deployment einer Nextcloud-Instanz mit MariaDB als Datenbank. Das Projekt wurde als Teil des Moduls M158 an einer Berufsschule entwickelt. Nextcloud ist eine Open-Source-Plattform zur Dateifreigabe und Kommunikation, die hier durch Docker-Container leicht zu deployen ist.

## Worum geht es in diesem Repository
Dieses Repository wurde im Rahmen des Moduls M158 in der Berufsschule erstellt und enthält die notwendigen Dateien, um Nextcloud mithilfe von Docker auszuführen. Das Setup beinhaltet zwei Dockerfiles: eines für MariaDB und eines für Nextcloud.

Nextcloud ist eine Open-Source-Lösung für das Speichern und Teilen von Dateien. Mit Docker können wir Nextcloud einfach in Containern betreiben, was die Verwaltung und Skalierung erleichtert. Dieses Repository enthält die notwendigen Dockerfiles, um eine Nextcloud-Instanz mit einer MariaDB-Datenbank zu betreiben.

## Voraussetzungen

Stellen sicher, dass folgende Software auf dem System installiert ist:
- Docker
- Docker Compose
- Git (zum Klonen dieses Repositories)

# Installation
### Repository klonen
- git clone https://gitlab.com/your-username/your-repository.git
- cd your-repository

#### Images bauen (Mit den Dockerfiles von diesem Repository)

Führen Sie die folgenden Befehle in Ihrem Terminal aus, um die Docker-Images zu erstellen:
- docker build -f Dockerfile.mariadb -t mariadb-image .
- docker build -f Dockerfile.nextcloud -t nextcloud-image .

### Zugriff auf Nextcloud
Der Zugriff auf Nextcloud erfolgt anschliessend über "http://localhost:8080".
